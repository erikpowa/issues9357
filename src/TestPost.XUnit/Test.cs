﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace TestPost.XUnit
{
    // ISSUE 9357
    public class Test
    {
        private static HttpClientHandler _handler = new HttpClientHandler()
        {
            Proxy = null,
            UseProxy = false,
            AllowAutoRedirect = true,
            UseCookies = true
        };

        /// <summary>
        /// Test without any setup, site http://followerspot.com/
        /// </summary>
        [Fact]
        public async void Test0()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://followerspot.com/");

                var formContent = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>("username", "placewhereiamfree")
                });

                var response = await client.PostAsync("/login.php", formContent, CancellationToken.None);

                if (response == null || !response.IsSuccessStatusCode)
                {
                    Assert.True(false, "server unavailable");
                }

                var responseText = await response.Content.ReadAsStringAsync();

                Assert.True(responseText.Contains("Log out"), "Failed to login");
            }
        }

        /// <summary>
        /// Test without any setup, site http://httpbin.org/
        /// </summary>
        [Fact]
        public async void Test1()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://httpbin.org/");

                var formContent = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>("key", "val")
                });

                var response = await client.PostAsync("/post", formContent, CancellationToken.None);

                if (response == null || !response.IsSuccessStatusCode)
                {
                    Assert.True(false, "server unavailable");
                }

                var responseText = await response.Content.ReadAsStringAsync();

                var model = JsonConvert.DeserializeObject<HttpbinModel>(responseText);

                Assert.True(model.form.key != null);
                Assert.True(model.form.key == "val");
            }
        }

        /// <summary>
        /// Test with manually added headers, site http://followerspot.com/
        /// </summary>
        [Fact]
        public async void Test2()
        {
            using (var client = new HttpClient(_handler))
            {
                // peek for default cookies
                var peek = await client.GetAsync("http://followerspot.com");

                if (peek == null || !peek.IsSuccessStatusCode)
                {
                    Assert.True(false, "server unavailable");
                }

                var formContent = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>("username", "placewhereiamfree")
                });

                HttpRequestMessage msg = new HttpRequestMessage();
                msg.Content = formContent;
                msg.Method = HttpMethod.Post;
                msg.RequestUri = new Uri("http://followerspot.com/login.php");
                // clear headers
                msg.Headers.Clear();
                msg.Headers.Add("Upgrade-Insecure-Requests", "1");
                msg.Headers.Add("DNT", "1");
                msg.Headers.Add("HOST", "followerspot.com");
                msg.Headers.Add("Origin", "http://followerspot.com");
                msg.Headers.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36");
                msg.Headers.Add("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
                msg.Headers.Add("Accept-Encoding", "gzip, deflate");
                msg.Headers.Add("Accept-Language", "en-US,en;q=0.8,hu;q=0.6,en-GB;q=0.4");
                msg.Headers.Add("Cache-Control", "max-age=0");
                msg.Headers.Add("Connection", "keep-alive");
                msg.Headers.Add("Cookie", "__cfduid=d94b5e60e4be0b41321999b0a6e4d35081464317946; PHPSESSID=6pnelqggps8rtfdbdglhpdtpp7; twitter_ad=1; _gat=1; _ga=GA1.2.1041151780.1464317948");
                msg.Headers.Add("Referer", "http://followerspot.com/index");

                // manually set contenttype
                msg.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/x-www-form-urlencoded");

                var response = await client.SendAsync(msg);

                if (response == null || !response.IsSuccessStatusCode)
                {
                    Assert.True(false, "server unavailable");
                }

                var responseText = await response.Content.ReadAsStringAsync();

                Assert.True(responseText.Contains("Log out"), "Failed to login");
            }
        }

        /// <summary>
        /// Test without any setup, site http://weplugit.com/
        /// </summary>
        [Fact]
        public async void Test3()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("http://weplugit.com/");

                var formContent = new FormUrlEncodedContent(new[] {
                    new KeyValuePair<string, string>("tumblr_username", "get-dropdead.tumblr.com"),
                    new KeyValuePair<string, string>("login_type", "tumblr")
                });

                var peekFirst = await client.GetAsync("/");

                if (peekFirst == null || !peekFirst.IsSuccessStatusCode)
                {
                    Assert.True(false, "server unavailable");
                }

                var login = await client.PostAsync("http://weplugit.com/login", formContent);

                if (login == null || !login.IsSuccessStatusCode)
                {
                    Assert.True(false, "server unavailable");
                }

                var loginText = await login.Content.ReadAsStringAsync();

                Assert.True(!string.IsNullOrEmpty(loginText), "invalid response");
                Assert.True(loginText.Contains("Logout"), "not logged in");
            }
        }
    }
}
