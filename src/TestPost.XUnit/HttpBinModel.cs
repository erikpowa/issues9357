﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestPost.XUnit
{
    public class Args
    {
    }

    public class Files
    {
    }

    public class Headers
    {
        [JsonProperty("Content-Length")]
        public string ContentLength {get; set;}

        [JsonProperty("Content-Type")]
        public string ContentType { get; set; }

        [JsonProperty("Accept-Encoding")]
        public string Encoding { get; set; }

        public string Host { get; set; }
    }

    public class HttpbinModel
    {
        public Args args { get; set; }
        public string data { get; set; }
        public Files files { get; set; }
        public dynamic form { get; set; }
        public Headers headers { get; set; }
        public object json { get; set; }
        public string origin { get; set; }
        public string url { get; set; }
    }
}
